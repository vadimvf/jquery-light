(function (root) {

  $l = function(arg) {
    if (typeof arg === "string"){
      var list = document.querySelectorAll(arg);
      list = [].slice.call(list);
      return new DOMNodeCollection(list);

    } else if (arg instanceof HTMLElement){
      arg = [].slice.call(arg);
      return new DOMNodeCollection(arg);
    } else if (typeof arg === "function") {
      document.addEventListener("DOMContentLoaded", arg);
    }
  };

  var DOMNodeCollection = function (array) {
    this.nodeEls = array;
  };

  DOMNodeCollection.prototype.html = function (string) {
    if (typeof html === "undefined"){
      return this.nodeEls[0].innerHTML;
    } else {
      for (var i = 0; i < this.length; i++){
        this.nodeEls[i].innerHTML = string;
      }
    }
  };

  DOMNodeCollection.prototype.empty = function () {
    this.html("");
  };

  DOMNodeCollection.prototype.append = function (arg) {
    if ( arg instanceof DOMNodeCollection ){
      for (var i = 0; i < this.nodeEls.length; i++){
        this.nodeEls[i].innerHTML += arg.nodeEls[0].outerHTML;
      }
    } else if(arg instanceof HTMLElement) {
      for (var j = 0; j < this.nodeEls.length; j++){
        this.nodeEls[j].innerHTML += arg.outerHTML;
      }
    } else {
      for (var k = 0; k < this.nodeEls.length; k++){
        this.nodeEls[k].innerHTML += arg;
      }
    }
  };

  DOMNodeCollection.prototype.addClass = function (string) {
    if (typeof string === "string" ) {
      this.nodeEls.forEach (function(el) {
        el.classList.add(string);
      });
    }
  };

  DOMNodeCollection.prototype.removeClass = function (string) {
    if (typeof string === "string" ) {
      this.nodeEls.forEach (function(el) {
        el.classList.remove(string);
      });
    } else if (string === undefined) {
      this.nodeEls.forEach (function(el) {
        while (el.classList.length > 0) {
          el.classList.remove(el.classList[0]);
        }
      });
    }
  };

  DOMNodeCollection.prototype.children = function () {
    var allChildren = [];
    this.nodeEls.forEach (function(el) {
      elChildren = [].slice.call(el.children);
      allChildren.push(elChildren);
    });
    allChildren.reduce(function(a,b) { return a.concat(b);});

    return new DOMNodeCollection(allChildren[0]);
  };

  DOMNodeCollection.prototype.parent = function () {
    var allParents = [];
    this.nodeEls.forEach (function(el) {
      elParent = [].slice.call(el.parentNode);
      allParents.push(elParent);
    });
    allParents.reduce(function(a,b) { return a.concat(b);});

    return new DOMNodeCollection(allParents);
  };

  DOMNodeCollection.prototype.find = function (selector){
    if (typeof selector === "string"){
      var list = [];
      this.nodeEls.forEach (function(el) {
        elFound = [].slice.call(el.querySelectorAll(selector));
        list.push(elFound);
      });
      list.reduce(function(a,b) { return a.concat(b);});

      return new DOMNodeCollection(list[0]);

    }
  };

  DOMNodeCollection.prototype.on = function (eventType, listener) {
      this.nodeEls.forEach (function(el) {
        el.addEventListener(eventType, listener);
      });
  };

  DOMNodeCollection.prototype.off = function (eventType, listener) {
      this.nodeEls.forEach (function(el) {
        el.removeEventListener(eventType, listener);
      });
  };

  root.$l = $l;

})(this);
